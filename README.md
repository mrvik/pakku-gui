# pakku-gui
This is a simple GTK-based interface to do basic operations with pakku as the AUR helper.
For the moment it has support to this actions:
   - Sync packages (Install)
   - Remove packages
   - Complete system upgrade (Sysupgrade)

Also it has some switches to:
   - Close the terminal when the process ends
   - Bypass confirmations (passing --noconfirm)
   - Skip AUR checks (passing --noaur to pakku)
   - Force install (passing --force to pakku) (not recommended)