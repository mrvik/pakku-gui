import os
import errno
from util import isTrue
from configparser import ConfigParser


class Preferences:
    def __init__(self):
        globalConfig = "/etc/pakku-gui.conf"
        self.userConfig = os.environ.get("HOME")+"/.config/pakku-gui.conf"
        self.testUserConfig()
        config = ConfigParser()
        config.read(globalConfig)
        config.read(self.userConfig)

        self.parser = config
        if 'main' not in config:
            self.createMain()
        self.config = config['main']
        self.internalOptions = ["autoclose"]

    def testUserConfig(self):
        dirname = os.path.dirname(self.userConfig)
        if not os.path.exists(dirname):
            try:
                os.makedirs(dirname)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

    def modify(self, k, v):
        self.parser.set("main", k, v)
        self.writefile()
        return v

    def writefile(self):
        with open(self.userConfig, "w") as configfile:
            self.parser.write(configfile)

    def createMain(self):
        self.parser.add_section("main")
        return self.writefile()

    def getOptions(self):
        out = ""
        for option in self.config:
            if option in self.internalOptions:
                continue
            if isTrue(self.config[option]):
                out += " --%s" % option
        return out


class desktopFile:
    def __init__(self):
        parser = ConfigParser()
        parser.read("/usr/share/applications/pakku-gui.desktop")
        self.parser = parser
        self.config = parser['Desktop Entry']
